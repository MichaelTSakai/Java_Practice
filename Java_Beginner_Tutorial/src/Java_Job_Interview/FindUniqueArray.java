/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Java_Job_Interview;

/**
 * Interview Question: Implement an algorithm to determine if a string has all
 * unique characters. What if you cannot use additional data structures? 
 * 
 * This implementation takes advantage of the fact that chars can be converted
 * to integer values. Then, I should use an array of boolean and use the 
 * characters themselves as index. The problem is that I don't know hoI made 
 * 150 spots in the array since the max value of a standard character on a laptop
 * is 126. This can add to the run time since you need 
 * to initialize a 150 character array. 
 * 
 * Note that Unicode can have up to 136,755 characters so that's a big array. 
 * The above note does not count towards my initial attempt since I didn't 
 * know about that.
 * 
 * The overall runtime, ignoring the initialization of the array, is O(n). 
 * 
 * The loop will, worst case scenario, run for n amount of times where n is the
 * length of the string. But search and editing the array values is O(1). I 
 * might need to brush up on the Big-O notations since I have a feeling this is
 * wrong.
 * 
 * @author micha
 */
public class FindUniqueArray {
    
    public static void main (String[] args)
    {
        String example = "qwertyuiop[]\\asdfghjkl;'zxcvbnm,./1234567890-=!@#$%^&*()_+`~QWERTYUIOP{}|ASDFGHJKL:\"ZXCVBNM<>?"; 
        boolean[] unique_char = new boolean[150];
        boolean end = false;
        for (int i = 0; i < example.length() && !end; i++)
        {
            if (unique_char[example.charAt(i)] == true)
            {
                System.out.print("This is not a Unique Character");
                end = true;
            }
            else
            {
                //Below prints out the integer value of the character. This 
                //is done to see the maximum value of characters one can write 
                //on a normal laptop. This shows that 150 characters are enough
                //to catch most characters (max I see is 126)
                //System.out.print((int)example.charAt(i) + " ");
                unique_char[example.charAt(i)] = true;
            }
        }
    }
    
}
